# Return Null on `NullPointerException`
When working with large API structures defined by schemas or any other means, traversing the object models to find the data you're interested in can be like playing Minesweeper. Every `.get*()` invocation is a stopping point to avoid `NullPointerException`s. Implementations either need to be wrapped in a conditional or the behavior wrapped in a try-catch block, i.e.:

**Conditional**:
```java
public Object getObjectOrNull(MyObject myObject) {
    if (myObject != null
        && myObject.getOne() != null
        && myObject.getOne().getTwo() != null) {
        return myObject.getOne().getTwo().getThree();
    } else {
        return null;
    }
}
```

**Try-Catch Block**:
```java
public Object getObjectOrNull(MyObject myObject) {
    try {
        return myObject.getOne().getTwo().getThree();
    } catch (NullPointerException npe) {
        return null;
    }
}
```

**@ReturnNullOnNpe**: \
This project aims at a solution similar to the latter while making the means for doing so easier for implementers, without the overhead of writing the same catch behavior multiple times:

```java
@ReturnNullOnNpe(CustomBehaviorBetweenNpeAndReturningNull.class)
public Object getObjectOrNull(MyObject myObject) {
    return myObject.getOne().getTwo().getThree();
}
```
where CustomBehaviorBetweenNpeAndReturningNull implements the behavior to be executed after throwing a `NullPointerException` and returning null:

```java
public class CustomBehaviorBetweenNpeAndReturningNull implements BehaviorBeforeReturningNull {
    @Override
    public void doBeforeReturningNull(Throwable thrown) {
        // Your catch block here
    }
}
```

## Maven Use
TBD

## Authors
Ryan Robison

## License
This project is currently unlicensed.