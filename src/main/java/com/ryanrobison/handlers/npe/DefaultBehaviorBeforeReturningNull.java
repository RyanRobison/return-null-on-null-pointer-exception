package com.ryanrobison.handlers.npe;

/**
 * Default behavior which prints the throwable's message to System.out
 */
public class DefaultBehaviorBeforeReturningNull implements BehaviorBeforeReturningNull {
    @Override
    public void doBeforeReturningNull(Throwable thrown, String extra) {
        System.out.println(thrown.getMessage());
    }
}
