package com.ryanrobison.handlers.npe;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is limited to methods (excluding constructors), i.e. not classes, interfaces, enums, fields,
 * parameters, local variables, annotation types, packages, type parameters, or uses of types.
 *
 * <p>
 * The annotation is retained at runtime so that the annotation can be reflectively analyzed at runtime for
 * (1) instantiating an instance of the annotation parameter <code>onNpe</code>, and
 * (2) invoking <code>onNpe()</code> when catching a NullPointerException
 * </p>
 *
 * @see ElementType
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ReturnNullOnNpe {

    /**
     * Strictly limited to implement the BehaviorBeforeReturningNull interface.
     *
     * <p>
     * The default behavior (DefaultBehaviorBeforeReturningNull) prints the throwable's message to System.out. An
     * alternative to this is SilentlyReturnNull.
     * </p>
     *
     * @see BehaviorBeforeReturningNull
     * @see DefaultBehaviorBeforeReturningNull
     * @see SilentlyReturnNull
     * @return
     */
    Class<? extends BehaviorBeforeReturningNull> value() default DefaultBehaviorBeforeReturningNull.class;

    /**
     * A String that is passed back to BehaviorBeforeReturningNull#doBeforeReturningNull
     *
     * @see BehaviorBeforeReturningNull#doBeforeReturningNull(Throwable, String)
     * @return
     */
    String extra() default "";
}
