package com.ryanrobison.handlers.npe;

import org.aspectj.lang.reflect.MethodSignature;

/**
 * Catch NullPointerExceptions in methods annotated with @ReturnNullOnNpe and return null. If the method executes
 * without throwing a NullPointerException, then that value is returned by default.
 *
 * @see ReturnNullOnNpe
 */
public aspect ReturnNullOnNpeAspect {

    Object around(): execution(@com.ryanrobison.handlers.npe.ReturnNullOnNpe * *(..)) {
        try {
            return proceed();
        } catch (NullPointerException npe) {
            try {
                MethodSignature signature = (MethodSignature) thisJoinPoint.getSignature();
                String methodName = signature.getMethod().getName();
                Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
                ReturnNullOnNpe annotation = thisJoinPoint.getTarget().getClass().getMethod(methodName, parameterTypes).getAnnotation(ReturnNullOnNpe.class);
                Class<? extends BehaviorBeforeReturningNull> behaviorDefinition = annotation.value();
                String stringExtra = annotation.extra();
                if (behaviorDefinition != null) {
                    BehaviorBeforeReturningNull behavior = behaviorDefinition.newInstance();
                    behavior.doBeforeReturningNull(npe, stringExtra);
                }
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException e) {
                System.out.println(e);
                return null;
            }
            return null;
        }
    }

}
