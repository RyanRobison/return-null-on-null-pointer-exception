package com.ryanrobison.handlers.npe;

/**
 * Interface for adding behavior in the case of having a NullPointerException thrown by the @ReturnNullOnNpe annotation.
 *
 * <p>
 * The behavior is added with compile time weaving and AspectJ. Because of how the behavior is injected into the aspect,
 * it must be stateless; the class used in the annotation is instantiated using reflection's Class#newInstance() method.
 * The class is then instantiated an doBeforeReturningNull is invoked.
 * </p>
 *
 * @see ReturnNullOnNpe
 * @see ReturnNullOnNpeAspect
 * @see Class#newInstance()
 */
public interface BehaviorBeforeReturningNull {

    /**
     * @param thrown Whatever is thrown is accessible to be used by any custom behavior, as seen fit for the implementation.
     * @param extra Specified by the user via the @ReturnNullOnNpe annotation's extra property
     * @see ReturnNullOnNpe#extra()
     */
    void doBeforeReturningNull(Throwable thrown, String extra);
}
