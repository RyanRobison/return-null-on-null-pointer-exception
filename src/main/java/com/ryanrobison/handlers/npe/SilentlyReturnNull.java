package com.ryanrobison.handlers.npe;

/**
 * A no-op alternative bundled behavior to the default behavior, which prints the throwable's message to System.out.
 */
public class SilentlyReturnNull implements BehaviorBeforeReturningNull {
    @Override
    public void doBeforeReturningNull(Throwable thrown, String extra) {
    }
}
