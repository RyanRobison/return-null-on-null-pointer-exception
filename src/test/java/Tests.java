import com.ryanrobison.handlers.npe.BehaviorBeforeReturningNull;
import com.ryanrobison.handlers.npe.ReturnNullOnNpe;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class Tests {

    public static class CustomBehaviorBeforeReturningNull implements BehaviorBeforeReturningNull {
        public static final String TEST_STRING = "Hello World!";
        @Override
        public void doBeforeReturningNull(Throwable thrown, String extra) {
            System.out.println(extra);
        }
    }

    static class TestClass {
        public String nonAnnotatedHello() {
            return makeNpeOccur();
        }

        @ReturnNullOnNpe
        public String annotatedHello() {
            return makeNpeOccur();
        }

        @ReturnNullOnNpe(
            value = CustomBehaviorBeforeReturningNull.class,
            extra = CustomBehaviorBeforeReturningNull.TEST_STRING)
        public String annotatedWithOnThrowsBehavior() {
            return makeNpeOccur();
        }

        public int getInt() {
            makeNpeOccur();
            return 0;
        }

        @ReturnNullOnNpe
        public int annotatedGetInt() {
            makeNpeOccur();
            return 0;
        }

        @ReturnNullOnNpe
        public void annotatedVoid() {
            makeNpeOccur();
        }

        private String makeNpeOccur() throws NullPointerException {
            String hello = null;
            return hello.toUpperCase();
        }
    }

    TestClass testClass = new TestClass();

    @Test(expected = NullPointerException.class)
    public void testNonAnnotatedThrows() {
        testClass.nonAnnotatedHello();
    }

    @Test
    public void testAnnotatedDoesNotThrow() {
        String hello = testClass.annotatedHello();
        assertNull(hello);
    }

    @Test
    public void testCustomBehaviorBeforeReturningNullOnAnnotatedMethod() {
        PrintStream standardOut = System.out;
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        String hello = testClass.annotatedWithOnThrowsBehavior();
        assertNull(hello);
        assertTrue(outputStream.toString().contains(CustomBehaviorBeforeReturningNull.TEST_STRING));
        System.setOut(standardOut);
        System.out.println(outputStream.toString());
    }

    @Test(expected = NullPointerException.class)
    public void testNonAnnotatedMethodThatReturnsAPrimaryType() {
        int value = testClass.getInt();
    }

    @Test
    public void testAnnotatedMethodThatReturnsAPrimaryType() {
        int value = testClass.annotatedGetInt();
        assertEquals(0, value);
    }

    @Test
    public void testAnnotatedVoidMethod() {
        testClass.annotatedVoid();
    }

}
